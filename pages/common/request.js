
//http://10.152.152.223:4053/swagger/index.html  接口地址
let baseUrl = 'http://10.152.153.108:4053/api/'

const postRequest = (url,data) =>{
	return new Promise((resolve,reject)=>{
		uni.showLoading({
			title:'加载中...',
			mask:true
		})
		uni.request({
			method:'POST',
			url:url,
			data:data,
			success:(res) => {
				uni.hideLoading();
				resolve(res.data)
			},
			fail:(err) =>{
				uni.hideLoading();
				uni.showToast({
				    title: err.data.msg,
				    duration: 2000
				});
			}
		})
	})
}


/**
     * Service/DrawWords
     * 抽取关键字
     * @param gender 性别
     */
    export function drawWords(param) {
        return postRequest(baseUrl + 'Service/DrawWords',{gender:param})
    }
	
	/**
	     * Service/ModulatePerfume
	     * 抽取关键字
	     * @param words 用户抽取的关键词
	     */
	    export function modulatePerfume(param) {
	        return postRequest(baseUrl + 'Service/ModulatePerfume',{words:param})
	    }
		
		/**
		     * Service/UserApplySave
		     * 抽取关键字
		     * @param name 姓名
			 * @param province 省
			 * @param city 城市
			 * @param counter 柜台
			 * @param mobile 手机号
		     */
		export function userApplySave(param) {
		    return postRequest(baseUrl + 'Service/UserApplySave',param)
		}